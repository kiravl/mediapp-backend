package com.mitocode.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mitocode.dao.ISignosDAO;
import com.mitocode.model.Signos;
import com.mitocode.service.ISignosService;

@Service
public class SignosServiceImpl implements ISignosService {
	
	@Autowired private ISignosDAO dao;

	@Override
	public void registrar(Signos signo) {
		this.dao.save(signo);
	}

	@Override
	public void modificar(Signos signo) {
		this.dao.save(signo);
	}

	@Override
	public void eliminar(long id) {
		this.dao.delete(id);
	}

	@Override
	public Page<Signos> listar(Pageable pageable) {
		return this.dao.findAll(pageable);
	}

	@Override
	public Signos get(long id) {
		return this.findOrNew(this.dao.findOne(id));
	}
	
	private Signos findOrNew(Signos signos) {
		if (signos == null) new Signos();
		return signos;
	}

}