package com.mitocode.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.mitocode.model.Signos;

public interface ISignosService {
	
	void registrar(Signos signo);

	void modificar(Signos signo);

	void eliminar(long id);

	Page<Signos> listar(Pageable pageable);
	
	Signos get(long id);

}